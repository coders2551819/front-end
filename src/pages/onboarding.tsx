import { Seo } from "@/ui/components/seo/seo";
import { Layout } from "@/ui/components/layout/layout";
import { REGISTERED } from "@/lib/session-status";
import { Session } from "@/ui/components/session/session";
import { OnboardingContainer } from "@/ui/modules/onboarding/onboarding.container";

export default function Onboarding() {
    return (
        <>
            <Seo title="Onboarding" description="Page de la page onboarding" />
            <Session sessionStatus={REGISTERED}>
                <OnboardingContainer />
            </Session>
        </>
    )
}
