// Component
import { Container } from "@/ui/components/container/container";
import { Layout } from "@/ui/components/layout/layout";
import { Navigation } from "@/ui/components/navigation/navigation";
import { Seo } from "@/ui/components/seo/seo";

// DESIGN SYSTEM
import { Avatar } from "@/ui/design-system/avatar/avatar";
import { Button } from "@/ui/design-system/button/button";
import { Logo } from "@/ui/design-system/logo/logo";
import { Spinner } from "@/ui/design-system/spinner/spinner";
import { Typography } from "@/ui/design-system/typography/typography";

// ICON
import { RiUser6Fill } from "react-icons/ri";

export default function DesignSystem() {
    return (
        <>
            <Seo title="Coders Gemini" description="Description..." />

            <Layout>
                <Container>
                    <div className="py-10 space-y-20">

                        <div className="space-y-5">
                            <Typography>Coders gemini</Typography>
                            <Typography theme="primary" variant="body-lg" component="h1">Coders gemini</Typography>
                            <Typography theme="secondary" variant="lead" component="div">Coders gemini</Typography>
                            <Typography variant="body-sm" component="div">Coders gemini</Typography>
                            <Typography variant="caption4" component="div">Coders gemini</Typography>
                            <Typography variant="caption4" weight="medium" component="div">Coders gemini</Typography>
                        </div>

                    </div>

                    <div className="flex items-start gap-7">
                        {/* Spinners */}
                        <div className="space-y-2">
                            <Typography variant="caption2" weight="medium">
                                Spinners
                            </Typography>
                            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
                                <Spinner size="small" />
                                <Spinner />
                                <Spinner size="large" />
                            </div>
                        </div>
                        {/* Avatars */}
                        <div className="space-y-2">
                            <Typography variant="caption2" weight="medium">
                                Avatars
                            </Typography>
                            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
                                <Avatar src="/assets/svg/avatar.svg" alt="leopard image" size="small" />
                                <Avatar src="/assets/svg/avatar.svg" alt="leopard image" />
                                <Avatar src="/assets/images/ai-generated-g23c808e6d_1280.jpg" alt="poseidon image" size="large" />
                                <Avatar src="/assets/images/poseidon-gdd923466a_1280.png" alt="poseidon image" size="large" />
                            </div>
                        </div>

                        {/* Logos */}
                        <div className="space-y-2">
                            <Typography variant="caption2" weight="medium">
                                Logos
                            </Typography>
                            <div className="flex items-center gap-2 p-5 border border-gray-400 rounded">
                                <Logo size="very-small" />
                                <Logo size="small" />
                                <Logo />
                                <Logo size="large" />
                            </div>
                        </div>
                    </div>

                    <div className="flex items-center gap-4 p-10">
                        <Button isLoading size="small">Accent</Button>
                        <Button isLoading size="small" icon={{ icon: RiUser6Fill }} iconPosition="left">Accent</Button>
                        <Button isLoading size="small" icon={{ icon: RiUser6Fill }} >Accent</Button>
                        <Button isLoading size="small" variant="secondary">Accent</Button>
                        <Button isLoading size="small" variant="outline">Outline</Button>
                        <Button isLoading size="small" variant="disabled" disabled>Disabled</Button>
                        <Button isLoading size="small" variant="ico" icon={{ icon: RiUser6Fill }} />
                    </div>

                    <div className="flex items-center gap-4 p-10">
                        <Button size="small">Accent</Button>
                        <Button size="small" icon={{ icon: RiUser6Fill }} iconPosition="left">Accent</Button>
                        <Button size="small" icon={{ icon: RiUser6Fill }} >Accent</Button>
                        <Button size="small" variant="secondary">Accent</Button>
                        <Button size="small" variant="outline">Outline</Button>
                        <Button size="small" variant="disabled" disabled>Disabled</Button>
                        <Button size="small" variant="ico" icon={{ icon: RiUser6Fill }} />
                    </div>

                    <div className="flex items-center gap-4 p-10">
                        <Button>Accent</Button>
                        <Button variant="secondary">Accent</Button>
                        <Button variant="outline">Outline</Button>
                        <Button variant="disabled" disabled>Disabled</Button>
                        <Button variant="ico" icon={{ icon: RiUser6Fill }} />
                    </div>

                    <div className="flex items-center gap-4 p-10">
                        <Button size="large">Accent</Button>
                        <Button size="large" variant="secondary">Accent</Button>
                        <Button size="large" variant="outline">Outline</Button>
                        <Button size="large" variant="disabled" disabled>Disabled</Button>
                        <Button size="large" variant="ico" icon={{ icon: RiUser6Fill }} iconTheme="secondary" />
                        <Button size="large" variant="ico" icon={{ icon: RiUser6Fill }} iconTheme="gray" />
                        <Button size="large" variant="ico" icon={{ icon: RiUser6Fill }} />

                    </div>
                </Container>
            </Layout>

        </>
    )
}