import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBerFSike5VkirXPpKIhCE9wBBK3su7CpA",
  authDomain: "codersgemini.firebaseapp.com",
  projectId: "codersgemini",
  storageBucket: "codersgemini.appspot.com",
  messagingSenderId: "840742253642",
  appId: "1:840742253642:web:6aa1a387ee1f8e4db6a2b3"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const db = getFirestore(app);