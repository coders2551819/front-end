import { auth, db } from "@/config/firebase-config";
import { UserDocument, UserInterface } from "@/types/user";
import { User, onAuthStateChanged } from "firebase/auth";
import { doc, onSnapshot } from "firebase/firestore";
import { useEffect, useState } from "react";

export default function useFirebaseAuth() {
    const [authUser, setAuthUser] = useState<UserInterface | null>(null);
    const [authUserISLoadind, setAuthUserISLoadind] = useState<boolean>(true);
    
    const formatAuthUser = (user: UserInterface) => ({
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        emailVerified: user.emailVerified,
        phoneNumber: user.phoneNumber,
        photoURL: user.photoURL,
    });

    const getUserDocument = async (user: UserInterface) => {
        if (auth.currentUser) {
            // get document form firestore
            const documentRef = doc(db, "users", auth.currentUser.uid);
            const compactUser = user;
            onSnapshot(documentRef, async (doc) => {
                if (doc.exists()) {
                    compactUser.userDocument = doc.data() as UserDocument;
                }
                setAuthUser((prevAuthUser) => ({
                    ...prevAuthUser,
                    ...compactUser,
                }));
                setAuthUserISLoadind(false);
            });
        }
    }

    const authStateChanged = async (authState: UserInterface | User | null ) => {
        if (!authState) {
            setAuthUser(null);
            setAuthUserISLoadind(false);
            return;
        }
        setAuthUserISLoadind(true); 
        const formattedUser = formatAuthUser(authState);
        await getUserDocument(formattedUser)
    }

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, authStateChanged);
        return () => unsubscribe() // FOR PERFORMANCE
    }, [])

    return {
        authUser,
        authUserISLoadind
    }
}