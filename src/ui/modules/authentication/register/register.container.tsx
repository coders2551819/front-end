import { RegisterFormFieldsType } from "@/types/form";
import { RegisterView } from "./register.view"
import { SubmitHandler, useForm } from "react-hook-form";
import { firebaseCreateUser, sendEmailVerificationProcedure } from "@/api/authentication";

import { toast } from 'react-toastify';
import { useToggle } from "@/hooks/use-toggle";
import { firestoreCreateDocument } from "@/api/firestore";

export const RegisterContainer = () => {

    const {value: isLoading, setValue: setIsLoading} = useToggle({initial: false});

    const { register, control, handleSubmit, formState: { errors }, setError, reset } = useForm<RegisterFormFieldsType>();

    const handleCreateUserAuthentication = async ({ email, password, how_did_hear }: RegisterFormFieldsType) => {
        const { error, data } = await firebaseCreateUser(email, password);
        if (error) {
            setIsLoading(false);
            toast.error(error.message);
            return;
        }

        const userDocumentData  = {
            email:  email,
            how_did_hear: how_did_hear,
            uid: data.uid,
            creation_date: new Date(),
        }
        
        handleCreateUserDocument("users", data.uid, userDocumentData);
    }

    const handleCreateUserDocument = async (collectionName: string, documentID: string, document: object) => {
        const { error }  = await firestoreCreateDocument(collectionName, documentID, document);
        console.log('ERROR', error);

        if(error) {
            setIsLoading(false);
            toast.error(error.message);
            return;
        }

        toast.success("Bienvenue sur la plateforme");
        setIsLoading(false);
        reset();
        sendEmailVerificationProcedure();
    }

    const onSubmit: SubmitHandler<RegisterFormFieldsType> = async (formData) => {
        console.log('formData', formData);
        setIsLoading(true);

        const { email, password } = formData;

        if (password.length <= 5) {
            setError("password", {
                type: "manuel",
                message: "Ton mot de passe doit comporter au minimum 6 caractères",
            })
            return;
        }
        handleCreateUserAuthentication(formData);
    }

    return (
        <RegisterView
            form={{
                errors,
                register,
                handleSubmit,
                onSubmit,
                isLoading,
            }}
        />
    )
}