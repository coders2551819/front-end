import { useAuth } from "@/context/AuthUserContext";
import { GUEST, REGISTERED } from "@/lib/session-status";
import { SessionStatusType } from "@/types/session-status-type";
import { ScreenSpinner } from "@/ui/design-system/spinner/screen-spinner";
import { useRouter } from "next/router";

interface Props {
    children:  React.ReactNode;
    sessionStatus?: SessionStatusType
}

export const Session =({children, sessionStatus}: Props) => {
    const router = useRouter();
    const { authUserISLoadind, authUser} = useAuth();
    const onboardingIsCompleted = authUser?.userDocument?.onboardingIsCompleted
    const shouldRedirectToOnboarding = () => {
        return (
            (!authUserISLoadind) &&
            authUser &&
            !onboardingIsCompleted &&
            router.asPath !== "/onboarding"
        )
    };
    
    const shouldNotRedirectToOnboarding = () => {
        return (
            (!authUserISLoadind) &&
            authUser &&
            onboardingIsCompleted &&
            router.asPath === "/onboarding"
        )
    }

    if (shouldRedirectToOnboarding()) {
        router.push('/onboarding');
        return <ScreenSpinner />
    }

    if (shouldNotRedirectToOnboarding()) {
        router.push('/mon-espace');
        return <ScreenSpinner />
    }

    if (sessionStatus === GUEST && !authUserISLoadind) {
        if (!authUser) {
            return <>{children}</>
        } else {
            router.push('/mon-espace');
        }
    }

    if (sessionStatus === REGISTERED && !authUserISLoadind) {
        if (authUser) {
            return <>{children}</>
        } else {
            router.push('/connexion');
        }
    }

    if (!sessionStatus && !authUserISLoadind) {
        return <>{children}</>
    }
    return <ScreenSpinner /> 
}